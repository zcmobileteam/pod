
ZCPaymentSDK is a pure-Swift library that allows merchants to pay through zainCash wallet.

## Installing

ZCPaymentSDK supports `CocoaPods` and written in `Swift 5.0`.

### CocoaPods

Add the pod to your Podfile:

```ruby
pod 'ZCPaymentSDK'
```
And then run:
```ruby
pod install
```
After installing the cocoapod into your project import ZCPaymentSDK with

```ruby
import ZCPaymentSDK
```

## How To Use

Add the below Code to `AppDelegate` to setup the SDK

```swift
import ZCPaymentSDK

func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
// Override point for customization after application launch.

/// use isProduction = false for testing
setup(public_key: "Publick key", merchant_code: "Merchant Code", isProduction: false)

return true
}

```

To start the payment , Call the below Function
```swift
func startPayment(controller : UIViewController,payment_amount : Double,order_id: String, merchant_name: String, delegate : PaymentGatewayDelegate)
```
implementation example
```swift
PaymentGateway.startPayment(controller : self, payment_amount: 500, order_id: "1", merchant_name: "Merchant Name", delegate: self)
```

implemet the delegate methods for `PaymentGatewayDelegate`
```swift
/// error contains the error message of failure
func paymentDidFail(_ controller: UIViewController, _ error: String){}

/// On payment Success Transaction Object is returned that includes all needed information
func paymentDidFinish(_ controller: UIViewController, _ transactionId: Transaction){}
```

### License

ZCPaymentSDK is released under the MIT license. 
